import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'article' })
export class Article {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('varchar', { length: 512 })
  title: string;

  @Column('text')
  content: string;

  @Column('varchar')
  image_url: string;

  @CreateDateColumn({ type: 'timestamptz' })
  created_at: string;

  @UpdateDateColumn({ type: 'timestamptz' })
  updated_at: string;
}
