import { Injectable, UnauthorizedException } from '@nestjs/common';

import { SignupDTO } from './dto/signup.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { LoginDTO } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async singup(body: SignupDTO) {
    // validasi2
    const result = await this.userRepo.save(body);

    return result;
  }

  async login(dto: LoginDTO) {
    // Cari email dari db
    const foundUser = await this.userRepo.findOne({
      where: { email: dto.email },
    });

    if (!foundUser) {
      throw new UnauthorizedException('No user found');
    }

    // bcrypt compare
    if (foundUser.password !== dto.password) {
      throw new UnauthorizedException('No user found');
    }

    // Buat JWT
    const payload = { sub: foundUser.email };

    const token = await this.jwtService.signAsync(payload);

    return token;
    // return JWT
  }
}
