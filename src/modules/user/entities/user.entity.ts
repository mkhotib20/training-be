import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'auth_user' })
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  // default mandatory, default lenght : 256
  @Column('varchar', { length: 512 })
  full_name: string;

  @Column('varchar')
  @Index({ unique: true })
  email: string;

  @Column('varchar', { nullable: true })
  avatar: string;

  @Column('varchar', { nullable: true })
  password: string;

  @CreateDateColumn({ type: 'timestamptz' })
  created_at: string;

  @UpdateDateColumn({ type: 'timestamptz' })
  updated_at: string;
}
