import { Module } from '@nestjs/common';
import { UserController } from './user.controller';

import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    JwtModule.register({
      global: true,
      secret: 'supersecret123',
      signOptions: {
        expiresIn: '72h',
      },
    }),
  ],
  exports: [],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
