import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';

export class SignupDTO {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  full_name: string;

  @IsString()
  @IsOptional()
  avatar: string;

  @IsString()
  @Length(8)
  @IsNotEmpty()
  password: string;
}
