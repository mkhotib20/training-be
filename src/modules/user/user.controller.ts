import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { SignupDTO } from './dto/signup.dto';
import { LoginDTO } from './dto/login.dto';

// path parent
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  // method & path
  @Post('/signup')
  async signup(@Body() payload: SignupDTO) {
    const result = await this.userService.singup(payload);
    return result;
  }

  @Post('/login')
  async login(@Body() payload: LoginDTO) {
    const result = await this.userService.login(payload);

    return { token: result };
  }
}
